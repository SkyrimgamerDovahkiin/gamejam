using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using TMPro;

public class Timer : NetworkBehaviour
{
    [SyncVar(hook = nameof(OnMyVarChanged))]
    public float GameTimer = 300f;
    float minutes;
    float seconds;
    public GameObject MainMenu;
    public TextMeshProUGUI TimerText;
    public NetworkManager NetworkManager;

    void Awake()
    {
        Cursor.lockState = CursorLockMode.Confined;
    }

    void Update()
    {
        minutes = Mathf.Floor(GameTimer / 60);
        seconds = GameTimer % 60;

        if (isServer && NetworkManager.numPlayers == 2)
        {
            GameTimer -= Time.deltaTime;
        }

        if (GameTimer <= 0f)
        {
            GameTimer = 0f;
            Time.timeScale = 0f;
        }
    }

    public void OnMyVarChanged(float oldVal, float newVal)
    {
        TimerText.text = minutes + ":" + Mathf.RoundToInt(seconds);
        if (seconds <= 9)
           TimerText.text = minutes + ":0" + Mathf.RoundToInt(seconds);
    }
}
