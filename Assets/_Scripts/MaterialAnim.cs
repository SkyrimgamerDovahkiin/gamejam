using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialAnim : MonoBehaviour
{
    Material mat;
    GameObject prefab;
    // Start is called before the first frame update
    void Start()
    {
        mat = prefab.GetComponent<MeshRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        mat.SetFloat("Blend", 1f);
    }
}
