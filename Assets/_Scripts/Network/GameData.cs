using System.Collections;
using UnityEngine;
using TMPro;
using Mirror;

public class GameData : NetworkBehaviour
{
    public TextMeshProUGUI ScoreCounterPlayer1;
    public TextMeshProUGUI ScoreCounterPlayer2;
    public HealthSystem HealthSystem;
    public GameObject Victory;
    public GameObject Defeat;
    public Timer Timer;


    //[SyncVar(hook = nameof(OnScore1Changed))]
    [SyncVar]
    public int ScoreIntPlayer1;

    [SyncVar]
    public int ScoreIntPlayer2;
    // // Start is called before the first frame update
    // void Start()
    // {

    // }

    // Update is called once per frame
    void Update()
    {
        ScoreCounterPlayer1.text = ScoreIntPlayer1.ToString();
        ScoreCounterPlayer2.text = ScoreIntPlayer2.ToString();
    }

    // public void OnScore1Changed(int oldVal, int newVal)
    // {
    //     //if (ScoreCounterPlayer1 != null)
    //     //{
    //         if (isServer)
    //         {
    //             ScoreCounterPlayer1.text = ScoreIntPlayer1.ToString();
    //         }
    //     //}
    // }
}
