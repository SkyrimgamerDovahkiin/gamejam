using Mirror;
using UnityEngine;

public class MyNetworkManager : NetworkManager
{
    [SerializeField] GameObject players = null;
    //[SerializeField] GameObject Player1 = null;
    //[SerializeField] GameObject Player2 = null;
    [SerializeField] Transform playerSpawnLeft = null;
    [SerializeField] Transform playerSpawnRight = null;
    //public int numPlayers = 0;

    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        Transform start = numPlayers == 0 ? playerSpawnLeft : playerSpawnRight;
        players = Instantiate(playerPrefab, start.position, start.rotation);
        //Debug.Log(numPlayers + " num Players");
        // if (NetworkServer.connections.Count == 0f)
        // {
        //     NetworkServer.AddPlayerForConnection(conn, Player1);
        // }
        // else if (NetworkServer.connections.Count > 0f)
        // {
        //     NetworkServer.AddPlayerForConnection(conn, Player2);
        // }

        NetworkServer.AddPlayerForConnection(conn, players);

        MyNetworkPlayer player = conn.identity.GetComponent<MyNetworkPlayer>();

        player.SetDisplayName($"Player {numPlayers}");

        player.SetColor(new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)));
    }
}
