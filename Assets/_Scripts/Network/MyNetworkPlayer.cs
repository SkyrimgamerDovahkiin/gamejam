using Mirror;
using TMPro;
using UnityEngine;

public class MyNetworkPlayer : NetworkBehaviour
{
    [SerializeField] private TMP_Text nameText = null;
    [SerializeField] private Renderer colorRenderer = null;

    [SyncVar(hook = nameof(HandleDisplayNameUpdated))] [SerializeField] private string displayName = "Missing Name";

    [SyncVar(hook = nameof(HandleDisplayColorUpdated))] [SerializeField] private Color color = Color.black;

    #region Server

    [Server]
    public void SetDisplayName(string newDisplayName)
    {
        displayName = newDisplayName;
    }

    [Server]
    public void SetColor(Color newColor)
    {
        color = newColor;
    }

    #endregion

    #region Client

    private void HandleDisplayColorUpdated(Color oldColor, Color newColor)
    {
        colorRenderer.material.SetColor("_Color", newColor);
        print(colorRenderer.material.color);
    }

    private void HandleDisplayNameUpdated(string oldName, string newName)
    {
        nameText.text = newName;
    }

    #endregion
}
