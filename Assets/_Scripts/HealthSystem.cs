using UnityEngine;
using UnityEngine.UI;

public class HealthSystem : MonoBehaviour
{
    public float maxInk = 100;
    public float currentInk;
    public Image healthbar;

        void Start()
    {
        healthbar = GameObject.Find("HB").GetComponent<Image>();
        currentInk = maxInk;
        SetMaxInk(maxInk);
    }
    void Update()
    {
        if (currentInk < 0f)
        {
            currentInk = 0f;        
        }
        else if (currentInk > maxInk)
        {
            currentInk = maxInk;
        }
    }

    public void SetMaxInk(float ink)
    {
        healthbar.fillAmount = ink / maxInk;
    }

    public void SetInk(float ink)
    {
        healthbar.fillAmount = ink / maxInk;
    }

    public void RemoveInk(float ink)
    {
        currentInk -= ink * Time.deltaTime;
        SetInk(currentInk);
    }

    public void RemoveInkConstant(float ink)
    {
        currentInk -= ink;
        SetInk(currentInk);
    }

    public void RestoreInk(float ink)
    {
        SetInk(currentInk);
        currentInk += ink * Time.deltaTime;
    }
}
