using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using Cinemachine;


public class ThirdPerson_Movment : MonoBehaviour
{
    public CharacterController controller;
    public NetworkBehaviour NetworkBehaviour;
    public Transform Cam;
    public CinemachineFreeLook freeLook;
    SprayMechanic2 sprayMechanic;
    Animator animator;
    public float speed = 40f;
    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;
    bool isRunning;

    private void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked;
        NetworkBehaviour = GetComponent<NetworkBehaviour>();
        sprayMechanic = GetComponent<SprayMechanic2>();
        animator = GetComponentInChildren<Animator>();
        Cam = Camera.main.transform;
        freeLook = GameObject.Find("CM FreeLook1").GetComponent<CinemachineFreeLook>();
        if (NetworkBehaviour.isLocalPlayer)
        {
            var c = transform.Find("newcharacter").Find("Cube.002");
            freeLook.LookAt = c; //Cube002.transform;
            freeLook.Follow = c;
        }
        //Debug.Log(c.name);
    }

    void Update()
    {
        if (Time.timeScale == 0f || !NetworkBehaviour.isLocalPlayer) { return; }
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;
        if (direction.magnitude <= 0f)
        {
            isRunning = false;
        }
        else
        {
            isRunning = true;
        }
        animator.SetBool("isRunning", isRunning);

        if (direction.magnitude >= 0.1f)
        {
            if (sprayMechanic.canMove)
            {
                float targetAngel = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + Cam.eulerAngles.y;
                float angel = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngel, ref turnSmoothVelocity, turnSmoothTime);
                transform.rotation = Quaternion.Euler(0f, angel, 0f);

                Vector3 moveDir = Quaternion.Euler(0f, targetAngel, 0f) * Vector3.forward;
                controller.Move(moveDir.normalized * speed * Time.deltaTime);
            }

        }
        controller.Move(Vector3.down * 15f * Time.deltaTime);
    }
}
