using System.Collections;
using UnityEngine;
using TMPro;
using Mirror;


public class SprayMechanic2 : NetworkBehaviour
{
    [SerializeField] HealthSystem HealthSystem;
    [SerializeField] Transform downCastEmpty;
    [SerializeField] PlayerColor playerColor;
    RaycastHit hit;
    public GameObject[] graffities = new GameObject[8];
    public Color colorTintPlayer1;
    public Color colorTintPlayer2;
    float blendTime = 2f;
    float newBlend;
    Camera cam;
    public float waitTimeNotSprayed = 1f;
    public float waitTimeSprayed = 1.5f;
    public bool canMove = true;
    Material mat;
    bool sprayedWall = false;
    bool sprayedFloor = false;
    public Transform TriggerEmpty;
    public GameData GameData;
    public TextMeshProUGUI ScoreCounterPlayer1;
    public TextMeshProUGUI ScoreCounterPlayer2;
    public GameObject Victory;
    public GameObject Defeat;
    public int ScoreIntPlayer1;
    public int ScoreIntPlayer2;
    public Timer Timer;
    bool isInTriggerWall;
    bool isInTriggerFloor;
    Animator animator;
    Quaternion decalRot;
    public bool thingsFound;


    //TODO: GameData Score zum Schluss wieder dem eigenen zuordnen und vergleichen 
    void Awake()
    {
        if (!isServer)
        {
            GameData = GameObject.Find("GameData").GetComponent<GameData>();
        }
        cam = Camera.main;
        colorTintPlayer1 = Color.red;
        colorTintPlayer2 = Color.yellow;
        ScoreIntPlayer1 = 0;
        ScoreIntPlayer2 = 0;
        animator = GetComponentInChildren<Animator>();
        //ScoreCounterPlayer1 = GameData.ScoreCounterPlayer1;
        //ScoreCounterPlayer2 = GameData.ScoreCounterPlayer2;
        HealthSystem = GameData.HealthSystem;
        Victory = GameData.Victory;
        Defeat = GameData.Defeat;
        Timer = GameData.Timer;
    }

    void Update()
    {
        //Gewonnen oder Verloren text aktivieren
        if (Timer.GameTimer <= 0f)
        {
            ScoreIntPlayer1 = GameData.ScoreIntPlayer1;
            ScoreIntPlayer2 = GameData.ScoreIntPlayer2;

            if (ScoreIntPlayer1 > ScoreIntPlayer2)
            {
                Victory.SetActive(true);
            }
            else if (ScoreIntPlayer1 < ScoreIntPlayer2)
            {
                Defeat.SetActive(true);
            }
        }

        //Spray Mechanik
        if (Input.GetMouseButtonDown(0) && HealthSystem.currentInk > 0f && isLocalPlayer)
        {
            if (!(isInTriggerWall || isInTriggerFloor)) { return; }
            Vector3 raycastPos = new Vector3();
            Vector3 raycastDir = new Vector3();
            float distance;
            if (cam.transform.localEulerAngles.x > 43f && cam.transform.localEulerAngles.x < 50f)
            {
                sprayedFloor = true;
                sprayedWall = false;
                raycastPos = downCastEmpty.position;
                raycastDir = Vector3.down;
                distance = 10f;
                //Debug.DrawRay(downCastEmpty.position, Vector3.down * distance);
            }
            else
            {
                sprayedWall = true;
                raycastPos = transform.position + Vector3.up;
                raycastDir = transform.forward;
                distance = 2f;
            }
            if (Physics.Raycast(raycastPos, raycastDir, out hit, distance))
            {
                print("etwas getroffen");
                Debug.DrawRay(transform.position, transform.forward * hit.distance, Color.yellow, 3f);
                Vector3 decalPos = hit.transform.position + hit.normal * 0.22f;
                //Vector3 decalRot = hit.transform.rotation;
                string triggerName = sprayedWall ? "WallTrigger" : "FloorTrigger";
                Debug.Log(triggerName);
                TriggerEmpty = hit.collider.gameObject.transform.Find(triggerName);
                if (TriggerEmpty == null) { return; }
                if (TriggerEmpty.childCount != 0)
                {
                    //MeshRenderer rend = TriggerEmpty.GetComponentInChildren<MeshRenderer>();
                    //if (TriggerEmpty.GetComponentInChildren<MeshRenderer>().material.GetColor("Color_f4eae5f884144459b2f28cee65ec7150") == colorTintPlayer2)
                    Sprayable sprayable = TriggerEmpty.GetComponent<Sprayable>();
                    print(sprayable.playerColor);
                    if (sprayable == null) { return; }
                    if (!(sprayable.playerColor == this.playerColor))
                    {
                        //ScoreIntPlayer2--;
                    }
                    else if (sprayable.playerColor == this.playerColor) { return; }
                    foreach (Transform child in TriggerEmpty.transform)
                    {
                        Destroy(child.gameObject);
                    }
                }
                bool hasChild = TriggerEmpty.childCount != 0;
                if (sprayedFloor)
                {
                    decalRot = Quaternion.LookRotation(-transform.forward) * Quaternion.LookRotation(hit.normal);
                }
                if (sprayedWall)
                {
                    decalRot = Quaternion.identity * Quaternion.LookRotation(hit.normal);
                }
                decalRot *= Quaternion.Euler(0f, 180f, 0f);
                int randIdx = Random.Range(0, graffities.Length);
                GameObject instanciatedPrefab = Instantiate(graffities[randIdx], decalPos, decalRot, TriggerEmpty);
                if (sprayedFloor)
                {
                    var vec = decalRot.eulerAngles;
                    vec.y = Mathf.Round(instanciatedPrefab.transform.rotation.eulerAngles.y / 90) * 90;
                    Vector3 rotVec = vec;
                    instanciatedPrefab.transform.rotation = Quaternion.Euler(rotVec);
                }
                mat = instanciatedPrefab.GetComponent<MeshRenderer>().material;
                //print(instanciatedPrefab.name);
                TriggerEmpty.GetComponent<Sprayable>().playerColor = this.playerColor;
                StartCoroutine(Wait(hasChild));
            }
        }
    }
    IEnumerator Wait(bool hasChild)
    {
        print($"Sprayed Wall is: {sprayedWall}");
        if (sprayedWall)
        {
            animator.SetTrigger("SprayWall");
            //animator.Play("SprayWall");
        }
        else if (sprayedFloor)
        {
            animator.SetTrigger("SprayFloor");
            //animator.Play("SprayFloor");
        }
        canMove = false;
        float timer = 0f;
        float duration = waitTimeNotSprayed;

        if (hasChild) //wenn wall sprayed
        {
            duration = 1.5f;
        }
        print(duration);
        while (timer <= duration)
        {
            if (isServer)
            {
                mat.SetColor("Color_f4eae5f884144459b2f28cee65ec7150", colorTintPlayer1);
            }
            else
            {
                mat.SetColor("Color_f4eae5f884144459b2f28cee65ec7150", colorTintPlayer2);
            }
            newBlend = Mathf.Lerp(-1f, 1f, timer / duration);
            timer += Time.deltaTime;
            mat.SetFloat("Vector1_2e2b38d8daf9420a8862f43ceada1dbf", newBlend);
            //print(newBlend);
            HealthSystem.RemoveInk(5f);
            yield return null;
        }

        if (isServer)
        {
            //wird 2 mal getan. warum?
            GameData.ScoreIntPlayer1++;
            Debug.Log("added Score Player1!");
        }
        else
        {
            Debug.Log("I Am not Server!");
            //GameData.ScoreIntPlayer2++;
            OnScore2Changed(GameData.ScoreIntPlayer2);
        }
        canMove = true;
        sprayedWall = false;
        sprayedFloor = false;
        animator.SetTrigger("BackToIdle");
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Wall"))
        {
            isInTriggerWall = true;
        }
        else if (other.CompareTag("Ground"))
        {
            isInTriggerFloor = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Wall"))
        {
            isInTriggerWall = false;
        }
        if (other.CompareTag("Ground"))
        {
            isInTriggerFloor = false;
        }
    }

    [Command]
    public void OnScore2Changed(int val)
    {
        GameData.ScoreIntPlayer2++;
    }
}

public enum PlayerColor
{
    None, Red, Yellow
}