using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Good_Plant_Red : MonoBehaviour
{
    public Image healthBar;
    public GameObject ParticleSystem;
    public GameObject[] InkFruits = new GameObject[3];
    public float HealingPoints = 5f;
    public HealthSystem HealthSystem;
    public float waitTimer = 5f;
    bool canHeal = true;

    private void Update()
    {
        // if (Input.GetButtonDown("Fire1"))
        // {
        //     HealthSystem.RemoveInkConstant(5f);
        // }
    }

    private void OnTriggerStay(Collider other)
    {
        if (canHeal)
        {
            HealthSystem.RestoreInk(HealingPoints);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
            StartCoroutine(Cooldown());
    }

    IEnumerator Cooldown()
    {
        canHeal = false;
        ParticleSystem.SetActive(false);
        for (int i = 0; i < InkFruits.Length; i++) { InkFruits[i].SetActive(false); }
        yield return new WaitForSeconds(waitTimer);
        canHeal = true;
        ParticleSystem.SetActive(true);
        for (int i = 0; i < InkFruits.Length; i++) { InkFruits[i].SetActive(true); }
        yield return null;
    }
}
