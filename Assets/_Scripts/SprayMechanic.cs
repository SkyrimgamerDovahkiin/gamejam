using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SprayMechanic : MonoBehaviour
{
    [SerializeField] Transform downCastEmpty;
    RaycastHit hit;
    public GameObject DecalEmpty;
    public GameObject[] graffities = new GameObject[8];
    public Color colorTintPlayer1;
    public Color colorTintPlayer2;
    float blendTime = 2f;
    float newBlend;
    Camera cam;
    public float waitTimeNotSprayed = 1f;
    public float waitTimeSprayed = 1.5f;
    public bool canMove = true;
    Material mat;
    bool sprayedWall = false;
    bool sprayedFloor = false;
    public Transform TriggerEmpty;
    public TextMeshProUGUI ScoreCounterPlayer1;
    public TextMeshProUGUI ScoreCounterPlayer2;
    public GameObject Victory;
    public GameObject Defeat;
    public int ScoreIntPlayer1;
    public int ScoreIntPlayer2;
    public Timer Timer;
    bool isInTriggerWall;
    bool isInTriggerFloor;


    void Start()
    {
        cam = Camera.main;
        colorTintPlayer1 = Color.red;
        colorTintPlayer2 = Color.yellow;
        ScoreIntPlayer1 = 0;
        ScoreIntPlayer2 = 0;
    }

    void Update()
    {
        ScoreCounterPlayer1.text = ScoreIntPlayer1.ToString();
        ScoreCounterPlayer2.text = ScoreIntPlayer2.ToString();
        if (Input.GetButtonDown("Fire1"))
        {
            if (isInTriggerWall)
            {
                sprayedWall = true;
                Debug.Log("sprayed Wall");
            }
            else if (isInTriggerFloor)
            {
                sprayedFloor = true;
                Debug.Log("sprayed Floor");
            }
        }

        if (Timer.GameTimer <= 0f)
        {
            if (ScoreIntPlayer1 > ScoreIntPlayer2)
            {
                Victory.SetActive(true);
            }
            else if (ScoreIntPlayer1 <= ScoreIntPlayer2)
            {
                Defeat.SetActive(true);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Wall"))
        {
            isInTriggerWall = true;
        }
        else if (other.CompareTag("Ground"))
        {
            isInTriggerFloor = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Wall"))
        {
            isInTriggerWall = false;
        }
        if (other.CompareTag("Ground"))
        {
            isInTriggerFloor = false;
        }
    }

    void OnTriggerStay(Collider other)
    {
        //Debug.Log("Trigger");
        // decide Raycast pos and dir by camera angle
        Vector3 raycastPos = new Vector3();
        Vector3 raycastDir = new Vector3();
        float distance;
        if (cam.transform.localEulerAngles.x > 43f && cam.transform.localEulerAngles.x < 50f)
        {
            raycastPos = downCastEmpty.position;
            raycastDir = Vector3.down;
            distance = 10f;
            Debug.DrawRay(downCastEmpty.position, Vector3.down * distance);
        }
        else
        {
            raycastPos = transform.position;
            raycastDir = transform.forward;
            distance = 2f;
        }
        if (Physics.Raycast(raycastPos, raycastDir, out hit, distance))
        {
            Debug.DrawRay(transform.position, transform.forward * hit.distance, Color.yellow);
            Vector3 decalPos = hit.collider.transform.position + hit.normal;
            //TriggerEmpty = hit.collider.gameObject.transform.GetChild(0);
            string triggerName = sprayedWall ? "WallTrigger" : "FloorTrigger";
            TriggerEmpty = hit.collider.gameObject.transform.Find(triggerName);
            print($"hit.collider name: {hit.collider.name}, trigger empty name: {TriggerEmpty.name}");
            if (sprayedWall || sprayedFloor)
            {
                //Debug.Log("sprayed!");
                if (TriggerEmpty == null) { return; }
                if (TriggerEmpty.childCount != 0)
                {
                    MeshRenderer rend = TriggerEmpty.GetComponentInChildren<MeshRenderer>();
                    if (TriggerEmpty.GetComponentInChildren<MeshRenderer>().material.GetColor("Color_f4eae5f884144459b2f28cee65ec7150") == colorTintPlayer2)
                    {
                        ScoreIntPlayer2--;
                    }
                    foreach (Transform child in TriggerEmpty.transform)
                    {
                        Destroy(child.gameObject);
                    }
                }
                bool hasChild = TriggerEmpty.childCount != 0;
                Quaternion decalRot = Quaternion.identity * Quaternion.LookRotation(hit.normal);
                decalRot *= Quaternion.Euler(0f, 180f, 0f);
                int randIdx = Random.Range(0, graffities.Length);
                GameObject instanciatedPrefab = Instantiate(graffities[randIdx], decalPos, decalRot, TriggerEmpty);
                mat = instanciatedPrefab.GetComponent<MeshRenderer>().material;
                print(instanciatedPrefab.name);
                StartCoroutine(Wait(hasChild));
                sprayedWall = false;
                sprayedFloor = false;
            }
        }
    }

    IEnumerator Wait(bool hasChild)
    {
        canMove = false;
        float timer = 0f;
        float duration = waitTimeNotSprayed;

        if (hasChild) //wenn wall sprayed
        {
            duration = 1.5f;
        }
        print(duration);
        while (timer <= duration)
        {
            mat.SetColor("Color_f4eae5f884144459b2f28cee65ec7150", colorTintPlayer1);
            newBlend = Mathf.Lerp(-1f, 1f, timer / duration);
            timer += Time.deltaTime;
            mat.SetFloat("Vector1_2e2b38d8daf9420a8862f43ceada1dbf", newBlend);
            //print(newBlend);
            yield return null;
        }
        ScoreIntPlayer1++;
        canMove = true;
    }
}
