using UnityEngine;

public class Bad_Plant : MonoBehaviour
{
    [SerializeField] HealthSystem HealthSystem;
    [SerializeField] float moveSpeed = 5f;
    [SerializeField] float rotSpeed = 5f;
    [SerializeField] LayerMask mask;
    [SerializeField] float stealAmount = 5f;
    Rigidbody rb;
    public Collider ArenaCollider;
    public float minDistance = 20f;
    public float stealDistance = 20f;
    public Vector3 RandomPos;

    GameObject player;

    void Awake()
    {
        player = GameObject.Find("Player_Red");
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 5f, mask))
        {
            Debug.DrawRay(transform.position, transform.forward * hit.distance, Color.yellow);
            float randAngle = Random.Range(15f, 60f);
            float sign = Random.value > 0.5f ? 1f : -1f;
            randAngle *= sign;
            transform.rotation *= Quaternion.Euler(0f, randAngle, 0f);
            //Debug.Log("Did Hit");
        }
        float distToPlayer = Vector3.Distance(player.transform.position, transform.position);
        if (distToPlayer < minDistance)
        {
            if (distToPlayer < stealDistance)
            {
                print(distToPlayer.ToString());
                HealthSystem.RemoveInkConstant(stealAmount);

                RandomPos = new Vector3(
                    Random.Range(ArenaCollider.bounds.min.x,ArenaCollider.bounds.max.x),
                    transform.position.y,
                    Random.Range(ArenaCollider.bounds.min.z, ArenaCollider.bounds.max.z)
                );
                transform.position = RandomPos;

                //TODO: Import Model and play anims
            }

            Quaternion targetRotQ = Quaternion.LookRotation((player.transform.position - transform.position));
            Vector3 targetRotE = targetRotQ.eulerAngles;
            //print($"targetRot euler: {targetRotE}");
            targetRotQ = Quaternion.Euler(0f, targetRotE.y, 0f);
            Quaternion newRot = Quaternion.RotateTowards(transform.rotation, targetRotQ, Time.deltaTime * rotSpeed);
            rb.MoveRotation(newRot);
        }
        rb.MovePosition(rb.position + transform.forward * Time.deltaTime * moveSpeed);
    }
}
