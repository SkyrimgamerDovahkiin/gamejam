﻿using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class FirstPersonController : MonoBehaviour
{
    public float MoveSpeed;
    public float vSpeed;
    public float jumpSpeed = 15f;
    public float MoveSpeedNormal = 3f;
    public float MoveSpeedSprint = 6f;
    public float RotSpeed = 4f;
    public float ClampAngle = 45f;
    float gravity = 9.81f;
    public float friction;
    public float JumpHeight = 3f;
    public Vector3 velocity;
    bool isGrounded;
    CharacterController cc;
    SprayMechanic sprayMechanic;
    //Transform camTransform;
    float xRot;
    float yRot;
    public GameObject player;
    bool isJumping;


    void Start()
    {
        cc = GetComponent<CharacterController>();
        //Camera cam = GetComponentInChildren<Camera>();
        //camTransform = cam.transform;
        sprayMechanic = GetComponent<SprayMechanic>();
        MoveSpeed = MoveSpeedNormal;
    }

    void Update()
    {
        isGrounded = cc.isGrounded;
        // Aufruf laufen, drehen, sprinten, springen
        // if (JumpAction.triggered && cc.isGrounded && hs.currentStamina > 0f)
        // {
        //     //player.transform.position.y += Mathf.Sqrt(JumpHeight * -3.0f * gravity);
        //     cc.Move(Vector3.up * vSpeed * Time.deltaTime);
        //     hs.RemoveStamina(10f);
        // }
        //wenn jump pressed dan jump
        Gravity();
        //CheckSprint();
        Move();
        //Rotate();
    }

    // Sprintfunktion
    // void CheckSprint()
    // {
    //     // Aufruf Sprint
    //     if (SprintAction.triggered)
    //     {
    //         MoveSpeed = MoveSpeedSprint;
    //     }
    //     else if (SprintAction.triggered && MoveSpeed == MoveSpeedSprint)
    //     {
    //         MoveSpeed = MoveSpeedNormal;
    //     }
    // }


    //Rotationsfunktion
    /*void Rotate()
    {
        Vector2 rotateVector = RotateAction.ReadValue<Vector2>();
        xRot += rotateVector.x;
        yRot -= rotateVector.y;
        xRot += Input.GetAxis("Vertical") * speed;
        yRot -= Input.GetAxis("Horizontal") * rotationSpeed;
        if (yRot > ClampAngle)
        {
            yRot = ClampAngle;
        }
        else if (yRot < -ClampAngle)
        {
            yRot = -ClampAngle;
        }
        player.transform.localRotation = Quaternion.Euler(-90f, xRot, 0f);
    }*/


    // Lauffunktion
    void Move()
    {
        //Vector3 fwdDir = -player.transform.up;
        //Vector3 sideDir = player.transform.right;
        //Vector2 moveVector = MoveAction.ReadValue<Vector2>();
        //Vector3 moveDir = fwdDir * moveVector.y + sideDir * moveVector.x;
        //velocity += moveDir * MoveSpeed * Time.deltaTime;
        //velocity -= friction * Time.deltaTime * velocity;
        float horizontal = Input.GetAxis("Horizontal") * MoveSpeed;
        float vertical = Input.GetAxis("Vertical") * MoveSpeed;
        if (sprayMechanic.canMove)
        {
            cc.Move((Vector3.right * horizontal + Vector3.forward * vertical) * Time.deltaTime);
        }
        //cc.Move(velocity);

        //anim abspielen
        // if (moveDir != Vector3.zero)
        // {

        // }
    }
    void Gravity()
    {
        if (cc.isGrounded)
        {
            vSpeed = 0f;
        }
        vSpeed += gravity * Time.deltaTime;
        velocity += Vector3.down * vSpeed * Time.deltaTime;
    }

    void Jump()
    {
        if (!cc.isGrounded) { return; }
        velocity.y = 0f;
        velocity += Vector3.up * jumpSpeed * Time.deltaTime;
    }
}
